# AVCodecSample

### 介绍
AVCodec 部件示例 Sample，基于 API12 构建，提供视频播放和录制的功能。
- 视频播放的主要流程是将视频文件通过解封装->解码->送显/播放。
- 视频录制的主要流程是相机采集->编码->封装成mp4文件。
### 播放支持的原子能力规格
| 媒体格式 | 封装格式                      | 码流格式                                 |
|------| :----------------------------|:-------------------------------------|
| 视频   | mp4                        | 视频码流：H.264/H.265, 音频码流:AudioVivid    |
| 视频   | mkv                        | 视频码流：H.264/H.265, 音频码流：aac/mp3/opus  |
| 视频   | mpeg-ts                    | 视频码流：H.264, 音频码流：AudioVivid          |

### 录制支持的原子能力规格

| 封装格式 | 视频编解码类型              | 
| -------- |----------------------|
| mp4      | H.264/H.265 | AAC、MPEG（MP3） |

注意，目前仅支持视频录制，未集成音频能力

### 效果预览
| 播放（横屏）| 播放（竖屏） | 录制 | 录制                                  |
|---------------------------------|--|-|-------------------------------------|
| ![播放.jepg](screenshots/播放.jpeg) | ![播放_2.jpeg](screenshots/播放_2.jpeg) | ![录制.jpeg](screenshots/录制.jpeg) | ![录制_2.jpeg](screenshots/录制_2.jpeg) |

### 使用说明

弹出是否允许“AVCodec”使用相机？点击“允许”

- 推送视频到文件管理？
    hdc file send xx.xx storage/media/100/local/files/Docs
- 推送视频到图库？
    hdc file send xx.mp4 storage/media/100/local/files
    hdc shell mediatool send /storage/media/100/local/files/xx.mp4
#### 播放

1. 点击下方“录制”，录制一个视频文件（无音频）或推送文件到本地(可单独音频、单独视频、视频含音频)

2. 点击播放按钮，选择从文件管理选取或从图库选取，点击确定，选择文件播放

#### 录制
1. （可选）选择相机分辨率

2. 点击“录制”

3. 选取视频输出路径

4. 点击“开始录制”

5. 点击“停止录制”

### 目录

仓目录结构如下：

```
video-codec-sample/entry/src/main/          
├── cpp                                # Native层
│   ├── capbilities                    # 能力接口和实现
│   │   ├── include                    # 能力接口
│   │   ├── audio_decoder.cpp          # 音频解码实现
│   │   ├── demuxer.cpp                # 解封装实现
│   │   ├── muxer.cpp                  # 封装实现
│   │   ├── video_decoder.cpp          # 视频解码实现
│   │   └── video_encoder.cpp          # 视频编码实现
│   ├── common                         # 公共模块
│   │   ├── dfx                        # 日志
│   │   ├── sample_callback.cpp        # 编解码回调实现   
│   │   ├── sample_callback.h          # 编解码回调定义
│   │   └── sample_info.h              # 功能实现公共类  
│   ├── render                         # 送显模块接口和实现
│   │   ├── include                    # 送显模块接口
│   │   ├── egl_core.cpp               # 送显参数设置
│   │   ├── plugin_manager.cpp         # 送显模块管理实现
│   │   └── plugin_render.cpp          # 送显逻辑实现
│   ├── sample                         # Native层
│   │   ├── player                     # Native层播放接口和实现
│   │   │   ├── Player.cpp             # Native层播放功能调用逻辑的实现
│   │   │   ├── Player.h               # Native层播放功能调用逻辑的接口
│   │   │   ├── PlayerNative.cpp       # Native层 播放的入口
│   │   │   └── PlayerNative.h         # 
│   │   └── recorder                   # Native层录制接口和实现
│   │       ├── Recorder.cpp           # Native层录制功能调用逻辑的实现
│   │       ├── Recorder.h             # Native层录制功能调用逻辑的接口
│   │       ├── RecorderNative.cpp     # Native层 录制的入口
│   │       └── RecorderNative.h       # 
│   ├── types                          # Native层暴露上来的接口
│   │   ├── libplayer                  # 播放模块暴露给UI层的接口
│   │   └── librecorder                # 录制模块暴露给UI层的接口
│   └── CMakeLists.txt                 # 编译入口       
├── ets                                # UI层
│   ├── common                         # 公共模块
│   │   └──utils                       # 共用的工具类
│   │       ├── CameraCheck.ets        # 相机能力查询
│   │       ├── DateTimeUtils.ets      # 获取当前时间
│   │       └── Logger.ts              # 日志工具
│   ├── CommonConstants.ets            # 参数常量
│   ├── entryability                   # 应用的入口
│   │   └── EntryAbility.ts            # 申请权限弹窗实现
│   ├── pages                          # EntryAbility 包含的页面
│   │   └── Index.ets                  # 首页/播放页面
│   └── sample                         # sample
│       └── recorder                   # 录制
│           └── Recorder.ets           # 录制页面
├── resources                          # 用于存放应用所用到的资源文件
│   ├── base                           # 该目录下的资源文件会被赋予唯一的ID
│   │   ├── element                    # 用于存放字体和颜色 
│   │   ├── media                      # 用于存放图片
│   │   └── profile                    # 应用入口首页
│   ├── en_US                          # 设备语言是美式英文时，优先匹配此目录下资源
│   └── zh_CN                          # 设备语言是简体中文时，优先匹配此目录下资源
└── module.json5                       # 模块配置信息
```

### 具体实现
#### *播放*
##### UI层
1. 在UI层[Index.ets](entry%2Fsrc%2Fmain%2Fets%2Fpages%2FIndex.ets)页面，用户点击播放按钮后，触发点击事件，调起selectFile()函数，该函数会调起文件管理的选择文件模块，拿到用户选取文件的路径。
2. 用户选择文件成功后，调起play()函数，该函数会根据上一步获取到的路径，打开一个文件，并获取到该文件的大小，改变按钮状态为不可用，之后调起js层暴露给应用层的[playNative()](entry%2Fsrc%2Fmain%2Fcpp%2Ftypes%2Flibplayer%2Findex.d.ts)接口
3. 根据playNative字段，调起[PlayerNative::Play](entry%2Fsrc%2Fmain%2Fcpp%2Fsample%2Fplayer%2FPlayerNative.cpp)函数，此处会注册播放结束的回调。
4. 播放结束时，Callback()中napi_call_function()接口调起，通知应用层，恢复按钮状态为可用。
##### JS层
1. 在[Init()](entry%2Fsrc%2Fmain%2Fcpp%2Fsample%2Fplayer%2FPlayerNative.cpp)中调用PluginManager()中的Export()方法，注册OnSurfaceCreatedCB()回调，当屏幕上出现新的Xcomponent时，将其转换并赋给单例类PluginManager中的pluginWindow_;
##### Native层
参考[开发者文档](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/media/avcodec)，以下是补充：
1. 解码器config阶段，OH_VideoDecoder_SetSurface接口的入参OHNativeWindow*，即为PluginManager中的pluginWindow_。
2. [Start时](entry%2Fsrc%2Fmain%2Fcpp%2Fsample%2Fplayer%2FPlayer.cpp)，起两个专门用于输入和输出的线程。
3. 具体实现原理：
    - 解码器Start后，输入回调会调起，用户需要把待解码的数据填入OH_AVBuffer中，调用PushInputBuffer接口，归还解码器解码（每次Start后，至少要Push一次XPS帧)。
    - 解码器每解出来一帧，输出回调就会调起一次，用户需要及时调用送显或释放接口，归还buffer给解码器。
注意：由于解码器给用户的buffer数量有上限，用户需要及时归还回调上来的buffer给解码器，否则达到这个这个上限，解码器就会停止工作，直到有buffer被归还。
##### Buffer轮转
![img_3.png](img_3.png)
#### *录制*
##### UI层
1. 在UI层[Index.ets](entry%2Fsrc%2Fmain%2Fets%2Fpages%2FIndex.ets)页面，用户点击“开始录制”后，会调起文件管理，用户选择一个输出地址。录制结束后，文件会存放于此。
2. 选择好文件后，会用刚刚打开的fd，和用户预设的录制参数，掉起JS层的initNative，待初始化结束后，调用OH_NativeWindow_GetSurfaceId接口，得到NativeWindow的surfaceId，并把surfaceId回调回UI层。
3. UI层拿到编码器给的surfaceId后，调起页面路由，携带该surfaceId，跳转到[Recorder.ets](entry%2Fsrc%2Fmain%2Fets%2Frecorder%2FRecorder.ets)
4. 录制页面构建时，Xcomponent构建时，会调起.onLoad()方法，此方法，首先会拿到Xcomponent的surfaceId，然后调起createDualChannelPreview(),此函数会建立一个相机生产，Xcomponent和编码器的surface消费的生产消费模型。

##### Native层
参考[开发者文档](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/media/avcodec)，以下是补充：
1. 用户点击开始录制后，编码器启动，开始对UI层相机预览流进行编码。
2. 编码器每编码成功一帧，输出回调[OnNewOutputBuffer()](entry%2Fsrc%2Fmain%2Fcpp%2Fcommon%2Fsample_callback.cpp)就会调起一次，此时用户会拿到avcodec框架给出的编码后的OH_AVBuffer。
3. 在输出线程中，调用封装接口WriteSample，这一帧封装入MP4中了，之后调用释放接口，归还buffer给avcodec框架。

### 音画同步
#### 前言
##### 背景和目的
目前手机播放器在输出设备为蓝牙耳机时会出现严重音视频不同步现象， 严重影响用户体验。本文旨在指导第三方视频播放应用正确获取并使用音频相关信息来保证音视频同步。

精确的音视频同步是媒体播放的关键性能指标之一。一般来说，在录音设备上同时录制的音频和视频需要在播放设备（例如手机，电视，媒体播放器）上同时播放。为了实现设备上的音视频同步，可以按如下指南操作。
##### 概念定义
| Abbreviations缩略语 | Full spelling 英文全名      | Chinese explanation 中文解释 |
|------------------|:------------------------|:-------------------------|
| PTS              | Presentation Time Stamp | 送显时间戳                    |
| DTS              | Decoding Time Stamp     | 解码时间戳                    |
- DTS（解码时间戳）
    指音视频数据在解码器中开始解码的时间戳。它表示解码器应该从输入数据流中读取和解码的特定时间点。DTS用于控制解码器的解码顺序，确保音视频数据按照正确的顺序解码。
- PTS（显示时间戳）
    指音视频数据在播放时应该显示给用户的时间戳。它表示解码后的音视频数据在播放时应该出现在屏幕上或传递给音视频输出设备的时间点。PTS用于控制音视频的播放顺序和时序，以确保音视频在正确的时间点进行显示或播放。

##### 音画同步原理
音视频数据的最小处理单元称为帧。音频流和视频流都被分割成帧，所有帧都被标记为需要按特定的时间戳显示。音频和视频可以独立下载和解码，但就具有匹配时间戳的音频和视频帧应同时呈现，达到A/V同步的效果。
![img.png](img.png)
理论上，因为音频通路存在时延，匹配音频和视频处理，有三种A/V同步解决方案可用；
（1）连续播放音频帧：使用音频播放位置作为主时间参考，并将视频播放位置与其匹配。
（2）使用系统时间作为参考：将音频和视频播放与系统时间匹配。
（3）使用视频播放作为参考：让音频匹配视频。

| 策略名称        | 优点                                                  | 缺点                                                                       |
|-------------|:----------------------------------------------------|:-------------------------------------------------------------------------|
| 连续播放音频帧（推荐） | ①用户肉眼的敏感度较弱，不易察觉视频微小的调整。<br/>②容易实现，因为视频刷新时间的调整相对容易。 | ①如果视频帧率不稳定或延迟渲染大，可能导致视频卡顿或跳帧。                                            |
| 使用系统时间作为参考  | 可以最大限度地保证音频和视频都不发生跳帧行为。                             | ①需要额外依赖系统时钟，增加系统复杂性和维护成本。<br/>②系统时钟的准确性对同步效果影响较大，如果系统时钟不准确，可能导致同步效果大打折扣。 |
| 使用视频播放作为参考  | 音频可以根据视频帧进行调整，减少音频跳帧的情况。                            | ①音频播放可能会出现等待或加速的情况，相较于视频，会对用户的影响更为严重和明显。<br/>②如果视频帧率不稳定，可能导致音频同步困难。      |
第一个选项是唯一一个具有连续音频数据流的选项，没有对音频帧的显示时间、播放速度或持续时间进行任何调整。这些参数的任何调整都很容易被人的耳朵注意到，并导致干扰的音频故障，除非音频被重新采样；但是，重新采样也会改变音调。因此，一般的多媒体应用使用音频播放位置作为主时间参考。以下段落将讨论此解决方案。（其他两个选项不在本文档的范围内）
#### 效果展示
##### 场景说明
##### 适用范围
适用于应用中视频播放过程中，由于设备渲染延迟、播放链路异常导致的音画不同步的场景
##### 场景体验指标
音画同步标准
① 为了衡量音画同步的性能，用对应音频和视频帧实际播放时间的差值作为数值指标，数值大于0表示声音提前画面，小于0表示声音落后画面。
② 最大卡顿时长，单帧图像停滞时间超过100ms的，定义为卡顿一次。连续测试5分钟，建议设置为100ms。
③ 平均播放帧率，平均每秒播放帧数，不反映每帧显示时长。
测试基准：一倍速场景

|        | 范围             | 主观体验 |
|--------|:---------------|:-----|
| S标（建议） | [-80ms, 25ms]  | 无法察觉 |
| A标     | [-125ms, 45ms] | 能够察觉 |
| B标     | [-185ms, 90ms] | 能够察觉 |

| 描述   | 应用内播放视频，音画同步指标应满足[-125ms, 45ms]。 |
|------|:---------------------------------|
| 类型   | 规则                               |
| 适用设备 | 手机、折叠屏、平板                        |
| 说明   | 无                                |

#### 场景分析
##### 典型场景及优化方案
**典型场景描述**
应用内播放视频，音画同步指标应满足[-80ms, 25ms].
**场景优化方案**
该解决方案使用：
- 视频同步到音频（主流方案）
- 获取音频渲染进度动态调整视频渲染进度

最终实现音画同步[-80ms,25ms]的效果。

**图2 音画同步示意图**
![img_1.png](img_1.png)

#### 场景实现
##### 场景整体介绍
音频和视频的管道必须同时以相同的时间戳呈现每帧数据。音频播放位置用作主时间参考，而视频管道只输出与最新渲染音频匹配的视频帧。对于所有可能的实现，精确计算最后一次呈现的音频时间戳是至关重要的。OS提供API来查询音频管道各个阶段的音频时间戳和延迟。

音频管道支持查询最新呈现的时间戳，getTimeStamp()方法提供了一种简单的方法来确定我们要查找的值。如果时间戳可用，则audioTimestamp实例将填充以帧单位表示的位置，以及显示该帧时的估计时间。此信息可用于控制视频管道，使视频帧与音频帧匹配。
##### 接口说明
```cpp
/*
 * Query the the time at which a particular frame was presented.
 *
 * @since 10
 *
 * @param renderer Reference created by OH_AudioStreamBuilder_GenerateRenderer()
 * @param clockId {@link #CLOCK_MONOTONIC}
 * @param framePosition Pointer to a variable to receive the position
 * @param timestamp Pointer to a variable to receive the timestamp
 * @return Function result code:
 *         {@link AUDIOSTREAM_SUCCESS} If the execution is successful.
 *         {@link AUDIOSTREAM_ERROR_INVALID_PARAM}:
 *                                                 1.The param of renderer is nullptr;
 *                                                 2.The param of clockId invalid.
 *         {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} Execution status exception.
 */
OH_AudioStream_Result OH_AudioRenderer_GetTimestamp(OH_AudioRenderer* renderer,
    clockid_t clockId, int64_t* framePosition, int64_t* timestamp);
```
注意事项：

(1)OH_AudioRenderer_Start到真正写入硬件有一定延迟，因此该接口在OH_AudioRenderer_Start之后过一会儿才会再拿到有效值，期间音频未发声时建议画面帧先按照正常速度播放，后续再逐步追赶音频位置从而提升用户看到画面的起搏时延。

(2)当framePosition和timeStamp稳定之前，调用可以比较频繁(如100ms)，当以稳定的速度增长前进后，建议OH_AudioRenderer_GetTimestamp的频率不要太频繁，可以每分钟一次，最好不要低于500ms一次，因为频繁调用可能会带来功耗问题，因此在能保证音画同步效果的情况下，不需要频繁地查询时间戳。

(3)OH_AudioRenderer_Flush接口执行后，framePosition返回值会重新(从0)开始计算。

(4)OH_AudioRenderer_GetFramesWritten 接口在Flush的时候不会清空，该接口和OH_AudioRenderer_GetTimestamp接口并不建议配合使用。

(5)音频设备切换过程中OH_AudioRenderer_GetTimestamp返回的framePosition和timestamp不会倒退，但由于新设备写入有时延，会出现短暂时间内音频进度无增长，建议画面帧保持流程播放不要产生卡顿。

(6)OH_AudioRenderer_GetTimeStamp获取的是实际写到硬件的采样帧数，不受倍速影响。对AudioRender设置了倍速的场景下，播放进度计算需要特殊处理，系统保证应用设置完倍速播放接口后，新写入AudioRender的采样点才会做倍速处理。

##### 关键代码片段
(1)获取音频渲染的位置
```cpp
// get audio render position
int64_t framePosition = 0;
int64_t timestamp = 0;
int32_t ret = OH_AudioRenderer_GetTimestamp(audioRenderer_, CLOCK_MONOTONIC, &framePosition, &timestamp);
AVCODEC_SAMPLE_LOGI("VD framePosition: %{public}li, nowTimeStamp: %{public}li", framePosition, nowTimeStamp);
audioTimeStamp = timestamp; // ns
```
(2)音频启动前暂不做音画同步

- 音频未启动前，timestamp和framePosition返回结果为0，为避免出现卡顿等问题，暂不同步
```cpp
// audio render getTimeStamp error, render it
if (ret != AUDIOSTREAM_SUCCESS || (timestamp == 0) || (framePosition == 0)) {
    // first frame, render without wait
    videoDecoder_->FreeOutputBuffer(bufferInfo.bufferIndex, true);
    lastPushTime = std::chrono::system_clock::now();
    continue;
}
```
(3)根据视频帧pts和音频渲染位置计算延迟waitTimeUs
- audioPlayedTime音频帧期望渲染时间
- videoPlayedTime视频帧期望送显时间
```cpp
// after seek, audio render flush, framePosition = 0, then writtenSampleCnt = 0
int64_t latency = (writtenSampleCnt - framePosition) * 1000 * 1000 / sampleInfo_.audioSampleRate;
AVCODEC_SAMPLE_LOGI("VD latency: %{public}li writtenSampleCnt: %{public}li", latency, writtenSampleCnt);

nowTimeStamp = GetCurrentTime();
int64_t anchordiff = (nowTimeStamp - audioTimeStamp) / 1000;

int64_t audioPlayedTime = audioBufferPts - latency + anchordiff; // us, audio buffer accelerate render time
int64_t videoPlayedTime = bufferInfo.attr.pts;                   // us, video buffer expected render time

// audio render timestamp and now timestamp diff
int64_t waitTimeUs = videoPlayedTime - audioPlayedTime; // us
```
(4)根据业务延迟做音画同步策略
- [,-40ms) 视频帧较晚，此帧丢掉
- [-40ms,0ms)视频帧直接送显
- [0ms,)视频帧较早，根据业务需要选择现象追帧
```cpp
// video buffer is too late, drop it
if (waitTimeUs < WAIT_TIME_US_THRESHOLD_WARNING) {
    dropFrame = true;
    AVCODEC_SAMPLE_LOGI("VD buffer is too late");
} else {
    AVCODEC_SAMPLE_LOGE("VD buffer is too early waitTimeUs:%{public}ld", waitTimeUs);
    // [0, ), render it wait waitTimeUs, max 1s
    // [-40, 0), render it
    if (waitTimeUs > WAIT_TIME_US_THRESHOLD) {
        waitTimeUs = WAIT_TIME_US_THRESHOLD;
    }
    // per frame render time reduced by 33ms
    if (waitTimeUs > sampleInfo_.frameInterval + PER_SINK_TIME_THRESHOLD) {
        waitTimeUs = sampleInfo_.frameInterval + PER_SINK_TIME_THRESHOLD;
        AVCODEC_SAMPLE_LOGE("VD buffer is too early and reduced 33ms, waitTimeUs: %{public}ld", waitTimeUs);
    }    
}
```
(5)进行音画同步
若视频帧的时间大于2倍vsync的时间，则需要sleep超过的时间。
```cpp
if (static_cast<double>(waitTimeUs) > FRAME_TIME * LIP_SYNC_BALANCE_VALUE) {
    std::this_thread::sleep_for(std::chrono::microseconds(
        static_cast<int64_t>(static_cast<double>(waitTimeUs)-FRAME_TIME * LIP_SYNC_BALANCE_VALUE)));
}
ret = videoDecoder_->FreeOutputBuffer(bufferInfo.bufferIndex, !dropFrame, FRAME_TIME * LIP_SYNC_BALANCE_VALUE * 1000 + GetCurrentTime());
CHECK_AND_BREAK_LOG(ret == AVCODEC_SAMPLE_ERR_OK, "Decoder output thread");
```

### 相关权限
#### [ohos.permission.CAMERA](https://docs.openharmony.cn/pages/v3.2/zh-cn/application-dev/security/permission-list.md/#ohospermissioncamera)

### 依赖
不涉及。

### 约束与限制

1.本示例仅支持标准系统上运行;

2.本示例仅支持 API12 及以上版本SDK,SDK版本号(API Version 12 Release)，镜像版本号(5.0 Release)；

3.本示例需要使用DevEco Studio 5.0 才可编译运行。

### 相关仓
- [multimedia_av_codec](https://gitee.com/openharmony/multimedia_av_codec)